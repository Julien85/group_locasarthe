-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : ven. 19 juin 2020 à 11:50
-- Version du serveur :  10.1.44-MariaDB-0ubuntu0.18.04.1
-- Version de PHP : 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `locasarthe_eval`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id_client` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `code_postal` char(5) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `telephone` char(14) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `date_naissance` date NOT NULL,
  `numero_permis` varchar(12) NOT NULL,
  `date_permis` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id_client`, `nom`, `prenom`, `adresse`, `code_postal`, `ville`, `telephone`, `email`, `date_naissance`, `numero_permis`, `date_permis`) VALUES
(1, 'Boorne', 'Mozes', '9 Clemons Center', '78410', 'Corpus Christi', '0361579491', 'mboorne0@sogou.com', '1988-02-26', '864909485612', '2006-03-27'),
(2, 'Renad', 'Lindsey', '1811 Mayfield Street', '72323', 'Bjästa', '0295402048', 'lrenad2@bloglovin.com', '1966-03-02', '155614482795', '2006-03-27'),
(3, 'Goldsbury', 'Rena', '1 Nobel Hill', '43890', 'Turgenevo', '0789324295', 'rgoldsbury3@dell.com', '1981-02-27', '303915825585', '1989-05-25'),
(4, 'MacTrustey', 'Lois', '22182 John Wall Trail', '72270', 'Bensonville', '0676676368', 'lmactrustey1@jigsy.com', '1944-03-19', '043034157855', '1967-06-21'),
(5, 'Harbottle', 'Trip', '1575 Mockingbird Terrace', '35770', 'Edissiya', '0627528044', 'tharbottle4@foxnews.com', '1963-08-02', '799254328939', '1983-01-08'),
(6, 'Ingram', 'Harold', '29 Birchwood Park', '75011', 'Pasar Kulon', '0773566604', 'hingram5@cbc.ca', '1958-10-05', '054981785633', '1974-11-14'),
(7, 'Towl', 'Orson', '1 Schlimgen Parkway', '72200', 'Ressano Garcia', '0597198982', 'otowl6@webnode.com', '1994-01-23', '811580550096', '2018-05-21'),
(8, 'Stepney', 'Broddy', '52789 Marcy Avenue', '35580', 'Arcos', '0714405916', 'bstepney7@prnewswire.com', '1979-09-08', '280179986603', '1997-12-05'),
(9, 'Pizzie', 'Jennee', '27 Village Alley', '72140', 'Warnes', '0718306033', 'jpizzie8@nyu.edu', '1989-04-23', '820521124684', '2005-07-24'),
(10, 'Di Bartolomeo', 'Evvy', '9225 Comanche Point', '31304', 'Tomobe', '0765548430', 'edibartolomeo9@networkadvertising.org', '1990-08-21', '933754238674', '2009-04-30'),
(11, 'Cabane', 'Fernando', '6687 Florence Point', '72400', 'Huaikan', '0629953761', 'fcabanea@fc2.com', '1950-07-12', '675136447590', '1968-09-14'),
(12, 'Hummerston', 'Nissy', '782 Starling Point', '72410', 'Wuhu', '0612370417', 'nhummerstonb@smugmug.com', '1988-10-05', '647252693715', '2006-10-18'),
(13, 'Satcher', 'Winnie', '3463 Montana Crossing', '72580', 'Igbor', '0621600965', 'wsatcherc@japanpost.jp', '1976-12-21', '328689504050', '1996-12-27'),
(14, 'Spurier', 'Susette', '9374 Barby Court', '45306', 'Camino Real', '0604291583', 'sspurierd@pagesperso-orange.fr', '1970-04-02', '158876772642', '1988-09-30'),
(15, 'O\'Dennehy', 'Bernhard', '6346 Dixon Terrace', '05457', 'Carmen de Viboral', '0778170823', 'bodennehye@marketwatch.com', '1955-08-30', '299473319272', '1974-01-02'),
(16, 'Keaves', 'Elvira', '2 Westend Terrace', '46554', 'Carvalhal', '0419883991', 'ekeavesf@nps.gov', '1954-08-02', '783539719910', '1974-03-05'),
(17, 'Peirpoint', 'Cherice', '7725 Anzinger Park', '72120', 'Guabito', '0707517487', 'cpeirpointg@sourceforge.net', '1970-03-27', '782532123990', '1988-11-28'),
(18, 'Godfroy', 'Roger', '94970 Dixon Street', '72100', 'Huyuan', '813 523 1799', 'rgodfroyh@washington.edu', '1991-02-09', '950145052839', '1999-12-27'),
(19, 'De la Yglesias', 'Marlow', '0529 Hanover Center', '72000', 'Monte Patria', '0736194598', 'mdelayglesiasi@soundcloud.com', '1998-02-02', '801799323526', '2018-05-06'),
(20, 'Side', 'Roosevelt', '06533 6th Lane', '98124', 'Messina', NULL, NULL, '1958-12-15', '594584035904', '1988-11-04'),
(21, 'Lambrick', 'Octavia', '8 Roth Circle', '11424', 'Stockholm', '0111532970', 'olambrickk@umich.edu', '1948-07-26', '493175214020', '1973-06-04'),
(22, 'Keady', 'Danika', '166 Clemons Avenue', '88100', 'São José', '0499830904', 'dkeadyl@answers.com', '1964-09-02', '120843842376', '1994-01-09'),
(23, 'Nelsey', 'Pennie', '9786 Mifflin Circle', '68132', 'Przewóz', '0301563181', 'pnelseym@surveymonkey.com', '1951-06-15', '044538651461', '1972-05-20'),
(24, 'Wheelband', 'Chery', '23 Loftsgordon Drive', '61633', 'Logovskoye', '0624262830', 'cwheelbandn@photobucket.com', '1954-04-05', '914894715508', '1983-09-08'),
(25, 'Bullimore', 'Gordy', '75 Vahlen Plaza', '21058', 'Vievis', '0285602520', 'gbullimoreo@amazon.co.jp', '1960-09-25', '464067393522', '1980-02-01'),
(26, 'Banat', 'Ceciley', '24 Raven Hill', '73129', 'Oklahoma City', '0655872692', 'cbanatp@theguardian.com', '1990-12-14', '143758536662', '2010-07-20'),
(27, 'Robbeke', 'Jarred', '202 Autumn Leaf Avenue', '05504', 'San Andres', '0785195731', 'jrobbekeq@usgs.gov', '1968-04-30', '823336332275', '1990-07-03'),
(28, 'McKissack', 'Chris', '9614 Mayfield Way', '05831', 'Granica', '0109356412', 'cmckissackr@nyu.edu', '1998-08-01', '047131491107', '2017-02-08'),
(29, 'Fordham', 'Jemie', '92407 Dryden Center', '72000', 'Bosen', '0692046915', 'jfordhams@sohu.com', '1967-02-13', '936873379871', '2002-06-09'),
(30, 'Glacken', 'Lianne', '39 Fair Oaks Drive', '72700', 'Krajan', '0412876593', 'lglackent@kickstarter.com', '1996-10-28', '238236891219', '2008-01-04'),
(31, 'Marlow', 'Thaddus', '186 Westerfield Plaza', '72800', 'Lonpao Dajah', '0326571508', 'tmarlowu@printfriendly.com', '1974-02-16', '533231580481', '2008-06-02'),
(32, 'Mishaw', 'Nikos', '56 Mitchell Pass', '14109', 'Lisieux', '0421945898', 'nmishawv@ycombinator.com', '1989-11-30', '131259103572', '2007-12-30'),
(33, 'Stonebridge', 'Dyann', '81518 Stoughton Crossing', '14040', 'Negotino', '0657827466', 'dstonebridgew@google.co.jp', '1979-04-06', '383867036233', '1998-03-05'),
(34, 'Rodgers', 'Matthus', '5 Walton Park', '46365', 'Nové Město pod Smrkem', '0138369465', 'mrodgersx@bluehost.com', '2001-11-14', '704223396880', '2019-12-17'),
(35, 'Mercer', 'Jozef', '19605 Porter Center', '72000', 'Sidoger Lebak', '0693386645', 'jmercery@biglobe.ne.jp', '1990-08-22', '167384883693', '2019-12-17'),
(36, 'Miere', 'Aurea', '62577 Ridgeway Parkway', '72140', 'Huangtan', '0289933459', 'amierez@gnu.org', '1985-10-02', '673966133219', '2005-11-25'),
(37, 'Summerill', 'Zandra', '0400 Eggendart Parkway', '72240', 'Pagersari', '0714931511', 'zsummerill10@loc.gov', '1988-07-08', '014200772503', '2012-08-30'),
(38, 'Mityashin', 'Mirna', '96166 Loomis Junction', '72450', 'Baopukang', '0432132005', 'mmityashin11@senate.gov', '1965-03-19', '633555310894', '1988-07-20'),
(39, 'Wulfinger', 'Miriam', '68 Duke Parkway', '57201', 'Ząbkowice Śląskie', '0148868723', 'mwulfinger12@google.de', '1987-01-10', '038809330963', '2007-01-26'),
(40, 'MacVaugh', 'Susette', '6 Dawn Hill', '06214', 'Maastricht', NULL, NULL, '1973-02-17', '455711402034', '2003-08-27'),
(41, 'Hambright', 'Caitlin', '0747 Westerfield Way', '72580', 'Cungkal', '0276539392', 'chambright14@trellian.com', '1983-06-04', '314488040100', '2007-05-14'),
(42, 'Nichols', 'Garey', '1 Nancy Avenue', '72890', 'Gambo', '0235463123', 'gnichols15@webmd.com', '1957-03-06', '258178416354', '1999-05-04'),
(43, 'Rawlence', 'Gonzalo', '07508 Fisk Plaza', '06406', 'Victoria', '0177632891', 'grawlence16@over-blog.com', '1980-06-03', '115338883071', '1999-01-08'),
(44, 'Riditch', 'Jefferson', '1386 Shoshone Center', '72480', 'Cacocum', '0606322768', 'jriditch17@gravatar.com', '1971-09-27', '168759144694', '1989-12-04'),
(45, 'Schirach', 'Aharon', '7501 Sullivan Point', '68145', 'Niños Heroes', '0385690670', 'aschirach18@ted.com', '1945-01-16', '369812995147', '1972-08-16'),
(46, 'Trusler', 'Melita', '38 Hintze Crossing', '01214', 'Pirok', '0283753020', 'mtrusler19@psu.edu', '1997-06-29', '049539034347', '2008-07-04'),
(47, 'Suddock', 'Tad', '12 Sachs Road', '72850', 'Tirhanimîne', '0773970236', 'tsuddock1a@4shared.com', '1958-09-02', '703517154191', '1979-09-06'),
(48, 'Gilson', 'April', '651 Hintze Parkway', '49100', 'Emiliano Zapata', '0633031970', 'agilson1b@godaddy.com', '1973-07-31', '371481726371', '2000-04-08'),
(49, 'Mogie', 'Carola', '31377 Forster Junction', '06039', 'Tajao', '0524236942', 'cmogie1c@themeforest.net', '1995-11-13', '222308511652', '2011-03-17'),
(50, 'Birwhistle', 'Cristie', '43 Prentice Place', '10806', 'Oslo', '0342210505', 'cbirwhistle1d@disqus.com', '1977-09-25', '159412335027', '1998-07-10'),
(51, 'Pettingall', 'Penni', '4 Beilfuss Court', '72450', 'Sanxixiang', '0936841978', 'ppettingall1e@washingtonpost.com', '1958-06-23', '022218667657', '1981-01-14'),
(52, 'Botterman', 'Sheila-kathryn', '3 Gina Point', '72140', 'Cerna', '0453056500', 'sbotterman1f@wix.com', '1946-06-11', '434178973516', '1981-01-14'),
(53, 'Blackburne', 'Lee', '015 Magdeline Pass', '72540', '‘Arīqah', '0259709245', 'lblackburne1g@ustream.tv', '1951-05-21', '509697383683', '1977-08-27'),
(54, 'Brayford', 'Jean', '7 Dwight Hill', '49800', 'Naranjal', '0638168534', 'jbrayford1h@list-manage.com', '1956-08-27', '400846599634', '1977-08-27'),
(55, 'Dybald', 'Ford', '76555 Bonner Avenue', '53000', 'Buenos Aires', '0631969367', 'fdybald1i@hostgator.com', '1983-02-23', '967261480961', '2003-01-17'),
(56, 'Wheater', 'Raquela', '1562 Hermina Circle', '47724', 'Silveiros', '0591214657', 'rwheater1j@smugmug.com', '2000-12-11', '310157614105', '2019-12-16'),
(57, 'Cutridge', 'Delbert', '01799 Sommers Road', '72900', 'Bacun', '0305890911', 'dcutridge1k@pagesperso-orange.fr', '1963-01-30', '998151125970', '1992-06-15'),
(58, 'Boast', 'Urbanus', '0 Springview Park', '72430', 'Ulaan Khat', '0186671353', 'uboast1l@mashable.com', '1954-01-17', '475928893545', '1978-08-14'),
(59, 'Capron', 'Ruggiero', '93 Lawn Junction', '72358', 'Mehona', '0108900649', 'rcapron1m@usa.gov', '1998-10-27', '226657098935', '2015-03-08'),
(60, 'Newbatt', 'Shamus', '6588 Ridgeview Terrace', '49800', 'Gar', '0621472179', 'snewbatt1n@github.com', '1955-10-10', '026128933677', '1988-05-28'),
(61, 'Chown', 'Rivy', '3827 Westend Parkway', '72000', 'Qingduizi', '0272993750', 'rchown1o@sciencedirect.com', '1972-12-11', '201488771387', '1991-07-17'),
(62, 'Gregoriou', 'Robinette', '2640 Toban Circle', '49400', 'Debre Sīna', '0455861191', 'rgregoriou1p@sogou.com', '1984-01-16', '585406869779', '2003-06-07'),
(63, 'Heeney', 'Mario', '6 Haas Junction', '49430', 'Ḩātim', '0228987353', 'mheeney1q@answers.com', '1968-10-02', '417979483653', '1990-09-23'),
(64, 'Brighty', 'Aleda', '12201 Schlimgen Avenue', '72000', 'Garupá', '0727328258', 'abrighty1r@mail.ru', '1980-07-08', '789811738624', '1999-04-08'),
(65, 'Wherton', 'Giavani', '52680 Red Cloud Street', '72100', 'Banjar Peguyangan', '0592424145', 'gwherton1s@youtu.be', '1958-06-10', '914786951824', '1996-05-12'),
(66, 'Quincey', 'Mariya', '387 Morning Crossing', '45211', 'Leninogorsk', '0254560541', 'mquincey1t@flavors.me', '1997-06-15', '848989146011', '2010-07-18'),
(67, 'Brogiotti', 'Meaghan', '6231 Talmadge Center', '49200', 'Punākha', '0255324712', 'mbrogiotti1u@php.net', '1984-09-06', '783368509987', '2000-06-18'),
(68, 'Novelli', 'Celestyna', '5 Swallow Way', '72000', 'Watuagung', '0363625622', 'cnovelli1v@homestead.com', '1974-04-18', '914091368998', '1998-02-18'),
(69, 'Hansard', 'Frederik', '052 Hayes Place', '88115', 'Inowrocław', '0332912936', 'fhansard1w@google.com.au', '1993-07-16', '114621331996', '2014-10-15'),
(70, 'Bettinson', 'Tedra', '1032 Dayton Plaza', '72100', 'Kalangsari', '0135006842', 'tbettinson1x@samsung.com', '1961-02-04', '330566122321', '1982-01-07'),
(71, 'McQuillen', 'Berny', '39150 Sunnyside Junction', '35252', 'Slobodnica', '0669537072', 'bmcquillen1y@cisco.com', '2000-12-11', '937711513812', '2019-05-11'),
(72, 'Shave', 'Tommy', '462 Milwaukee Plaza', '14100', 'Dayu', '0122487578', 'tshave1z@wix.com', '1954-07-05', '911870090135', '2001-05-08'),
(73, 'Seakings', 'Rozele', '81552 Fairview Point', '12701', 'Loppi', '0162272489', 'rseakings20@un.org', '1971-10-20', '135614730695', '1992-10-11'),
(74, 'Mason', 'Sanson', '768 Fulton Street', '63270', 'Bagan', '0169031964', 'smason21@bluehost.com', '1974-06-15', '054784814366', '1998-10-14'),
(75, 'Halkyard', 'Michale', '620 Meadow Ridge Lane', '54236', 'Santa Valha', '0721157961', 'mhalkyard22@blogs.com', '1961-08-31', '261437329106', '1993-04-30'),
(76, 'Maffetti', 'Joshuah', '740 Fuller Circle', '11752', 'Yelizovo', '0305599595', 'jmaffetti23@apache.org', '1951-02-02', '394210603378', '1980-04-06'),
(77, 'Mitrikhin', 'Brandise', '06411 Springview Street', '12000', 'Bungursari', '0366280129', 'bmitrikhin24@unc.edu', '2000-08-27', '727543043704', '2019-04-05'),
(78, 'Mumbeson', 'Jessey', '7227 Nova Drive', '72420', 'El Cantón', '0996606466', 'jmumbeson25@eventbrite.com', '1962-01-05', '966369509821', '1993-06-07'),
(79, 'Poznan', 'Ursa', '13 Fremont Drive', '43229', 'Frydek', '0744455500', 'upoznan26@nymag.com', '1999-09-04', '375354592931', '2007-10-23'),
(80, 'de Copeman', 'Sibilla', '3007 Larry Road', '72500', 'Santa Rita', '0550266741', 'sdecopeman27@yolasite.com', '1978-12-19', '972147467649', '2000-07-06'),
(81, 'Carrack', 'Herman', '6 Sage Drive', '76001', 'Zlín', '0239675375', 'hcarrack28@hugedomains.com', '1993-03-21', '613225336653', '2008-11-17'),
(82, 'Fairholm', 'Simeon', '61916 Randy Hill', '75012', 'Pisangkemeng', '0930904095', 'sfairholm29@bravesites.com', '1975-03-13', '802381471540', '2004-01-12'),
(83, 'Trenear', 'Titos', '78673 Prairieview Court', '09203', 'Matungao', '0895046952', 'ttrenear2a@cmu.edu', '1969-02-11', '463663123643', '2003-06-08'),
(84, 'Turk', 'Lolita', '506 Blackbird Road', '34536', 'Béziers', '0684639683', 'lturk2b@bizjournals.com', '1978-07-28', '687885155934', '1999-08-04'),
(85, 'Martyntsev', 'Sinclare', '826 Sutherland Park', '05281', 'Junín', '0143632982', 'smartyntsev2c@instagram.com', '1991-05-06', '172047321611', '2009-10-08'),
(86, 'Heijnen', 'Beilul', '1235 Dawn Road', '30863', 'Bāndarban', '0945289642', 'bheijnen2d@macromedia.com', '1985-11-06', '781785353641', '2003-12-09'),
(87, 'McKune', 'Teresita', '1 Golden Leaf Plaza', '72000', 'Yuandianhui', '0564241478', 'tmckune2e@utexas.edu', '1955-12-02', '323440414860', '1981-07-25'),
(88, 'Tanswell', 'Meg', '13 Londonderry Pass', '72000', 'Xiaocun', '0492844307', 'mtanswell2f@cbc.ca', '1973-04-28', '034560309921', '1996-12-04'),
(89, 'Hauger', 'Gleda', '2 Westport Pass', '72100', 'Xiamayu', '0154496917', 'ghauger2g@nps.gov', '1969-01-06', '081188369859', '1995-07-08'),
(90, 'Girardeau', 'Jean', '5259 Twin Pines Court', '10409', 'Berlin', '0281856895', 'jgirardeau2h@ted.com', '1998-11-30', '011862096235', '2009-09-16'),
(91, 'Haville', 'Alfons', '174 Nobel Center', '72000', 'Slatyne', '0609351215', 'ahaville2i@ameblo.jp', '1984-02-17', '552503639737', '2002-11-06'),
(92, 'Kearley', 'Steffen', '40981 Hanson Drive', '01810', 'Tvøroyri', '0297456385', 'skearley2j@nytimes.com', '1961-10-25', '615294745940', '1993-07-06'),
(93, 'Cicccitti', 'Charlotte', '140 Bluejay Place', '72400', 'Kengkou', '0919846991', 'ccicccitti2k@senate.gov', '1953-04-23', '456627451490', '1988-05-07'),
(94, 'Joselin', 'Herculie', '6181 Fisk Crossing', '49284', 'Tennō', '0315026050', 'hjoselin2l@hud.gov', '1980-12-05', '529821495436', '1999-05-07'),
(95, 'Foston', 'Dottie', '6 Melrose Pass', '64234', 'Lescar', '0993285683', 'dfoston2m@tamu.edu', '1993-03-22', '068652041146', '2010-01-22'),
(96, 'Aronson', 'Tami', '419 American Ash Lane', '02049', 'Ikhtiman', '0243239737', 'taronson2n@flickr.com', '1989-02-16', '112561789675', '2018-04-09'),
(97, 'Sparrow', 'Jemie', '2575 Mccormick Alley', '49350', 'Pingxiang', '0918235958', 'jsparrow2o@bbc.co.uk', '1963-01-02', '214869257414', '1996-06-08'),
(98, 'Grayling', 'Blondie', '676 Commercial Park', '72650', 'Teupasenti', '0284098752', 'bgrayling2p@webeden.co.uk', '1954-02-10', '102214895014', '1995-06-08'),
(99, 'Scohier', 'Arabel', '690 Rowland Pass', '72000', 'Sassandra', '0229309777', 'ascohier2q@xing.com', '1952-04-16', '705570233968', '1970-10-18'),
(100, 'Want', 'Hastings', '415 Columbus Avenue', '72000', 'Liutan', '0251945378', 'hwant2r@pbs.org', '1959-06-20', '657854158629', '1980-12-07'),
(101, 'Burrill', 'Tracie', '40 Orin Trail', '72100', 'Qaxbaş', '0815107171', 'tburrill2s@blog.com', '1984-09-29', '123642655892', '2002-11-25'),
(102, 'Childerhouse', 'Shawnee', '0 Hauk Trail', '14500', 'Hüremt', '0459422882', 'schilderhouse2t@list-manage.com', '1948-09-10', '144851011474', '2003-05-07'),
(103, 'Ponnsett', 'Cassondra', '7451 Clove Place', '49350', 'Rathwire', '0774584893', 'cponnsett2u@flickr.com', '1993-02-20', '100280796625', '2011-10-05'),
(104, 'Dike', 'Bellanca', '0069 Express Crossing', '79800', 'Dourados', '0673401802', 'bdike2v@google.com.hk', '1971-05-30', '640155854035', '1999-07-08'),
(105, 'Fackrell', 'Wyatan', '35332 Arizona Parkway', '49970', 'Passos', '0339024145', 'wfackrell2w@shutterfly.com', '1976-03-26', '147149086136', '1999-09-04'),
(106, 'Clemanceau', 'Brig', '82 Burrows Lane', '35996', 'Solnechnyy', '0454502300', 'bclemanceau2x@123-reg.co.uk', '1961-05-14', '925258249445', '1997-08-19'),
(107, 'Chadd', 'Felike', '40044 Oneill Hill', '58100', 'Dao', '0836237367', 'fchadd2y@disqus.com', '1961-02-13', '660038635916', '2005-10-14'),
(108, 'Cottis', 'Binni', '481 Lillian Park', '72000', 'Al Jamīmah', '0628846322', 'bcottis2z@biblegateway.com', '1963-12-31', '261455029631', '1982-08-01'),
(109, 'Annear', 'Arleyne', '8561 Nelson Circle', '80108', 'Barurao', '0373870814', 'aannear30@t-online.de', '1986-04-11', '055695716075', '2006-03-25'),
(110, 'Haresnape', 'Diane-marie', '5100 Stoughton Hill', '12000', 'Pakusari', '0861997084', 'dharesnape31@japanpost.jp', '1943-04-15', '495140027115', '1968-06-18'),
(111, 'Nutkins', 'Anjela', '36624 Vidon Plaza', '14100', 'Doibang', '0281945245', 'anutkins32@yellowpages.com', '1962-04-20', '967069361478', '1692-07-08'),
(112, 'Strodder', 'Price', '4518 Ridgeway Pass', '92220', 'Bugo', '0213284574', 'pstrodder33@shutterfly.com', '1944-09-14', '030147160106', '1966-03-07'),
(113, 'Stoffer', 'Kylie', '97007 6th Avenue', '72140', 'Myronivka', '0505262323', 'kstoffer34@gov.uk', '1990-12-09', '349952110370', '2011-01-16'),
(114, 'Ousby', 'Prudi', '1 Brentwood Terrace', '35319', 'Gostilitsy', '0733307678', 'pousby35@comsenz.com', '1999-03-28', '016000819296', '2007-06-17'),
(115, 'Fullun', 'Idette', '22948 Chive Drive', '34441', 'Niedzica', '0615145190', 'ifullun36@purevolume.com', '1967-06-15', '305254069893', '1996-04-02'),
(116, 'Goodhall', 'Dallis', '686 Eliot Circle', '25464', 'Roliça', NULL, NULL, '1995-06-20', '817366828858', '2018-01-09'),
(117, 'Hoy', 'Vivianna', '252 Cordelia Trail', '72100', 'Zhaojia', '0380884096', 'vhoy38@whitehouse.gov', '1959-03-29', '578141466045', '1978-05-16'),
(118, 'Huonic', 'Pavlov', '9 Hovde Terrace', '72240', 'Ngrowo', '0594887141', 'phuonic39@ucla.edu', '1953-05-10', '381429697678', '1981-11-07'),
(119, 'Morot', 'Mada', '4 Spaight Park', '83243', 'Szlachta', '0747629441', 'mmorot3a@constantcontact.com', '1959-04-21', '019859125554', '1981-07-12'),
(120, 'Benck', 'Paquito', '69242 Swallow Plaza', '72410', 'Tanenofunan', '0611792956', 'pbenck3b@instagram.com', '1998-03-18', '007816507895', '2017-02-20'),
(121, 'Afield', 'Tess', '53 Fairfield Junction', '72210', 'Binglincha', NULL, NULL, '1953-07-07', '100229583146', '1977-04-05'),
(122, 'Boice', 'Ardyce', '97 Acker Hill', '75010', 'Caldas Novas', '0724242207', 'aboice3d@pcworld.com', '1990-04-26', '081738387097', '1999-07-01'),
(123, 'MacAndrew', 'Joelle', '2 Magdeline Park', '68537', 'Albania', '0511131516', 'jmacandrew3e@umn.edu', '1989-04-18', '047474614014', '2002-08-09'),
(124, 'Joffe', 'Liva', '83 Truax Street', '72400', 'Ikar', '0136303254', 'ljoffe3f@uiuc.edu', '1987-08-10', '329359406574', '2008-08-08'),
(125, 'Hillam', 'Kirby', '965 Eastlawn Circle', '72540', 'Tash-Kumyr', '0251014308', 'khillam3g@house.gov', '1946-01-30', '065117386618', '1975-04-20'),
(126, 'Boneham', 'Hogan', '978 Lyons Avenue', '48053', 'Sande São Lourenço', '0619384590', 'hboneham3h@4shared.com', '1948-08-26', '081637506570', '1972-04-18'),
(127, 'Oldfield', 'Bryant', '932 Petterle Lane', '49500', 'Lugouqiao', '0115719425', 'boldfield3i@google.co.uk', '1982-12-20', '302209874469', '2000-12-30'),
(128, 'Lyttle', 'Den', '8 Pawling Park', '48750', 'Isfara', '0749187664', 'dlyttle3j@unc.edu', '1984-04-23', '819814718281', '2006-07-08'),
(129, 'Napoleone', 'Orin', '096 Golf Terrace', '78070', 'Duqu', '0172065314', 'onapoleone3k@businessinsider.com', '1948-06-16', '146499572632', '1976-08-12'),
(130, 'Rossiter', 'Lyda', '0814 Bay Junction', '13550', 'Tyresö', '0284321500', 'lrossiter3l@simplemachines.org', '1956-12-07', '883554570354', '1985-02-27'),
(131, 'Cattini', 'Barbette', '1 Fordem Avenue', '83120', 'Wichit', '0539135567', 'bcattini3m@cam.ac.uk', '1970-04-11', '157999282625', '1999-07-19'),
(132, 'Kinnoch', 'Alexis', '5 Elmside Center', '33170', 'Almafuerte', '0602782350', 'akinnoch3n@discovery.com', '1998-12-23', '004366383442', '2017-08-04'),
(133, 'Teather', 'Rockwell', '307 Blackbird Avenue', '47114', 'Kamenický Šenov', '0636283952', 'rteather3o@symantec.com', '1985-03-10', '066845969329', '2003-09-07'),
(134, 'Bilovus', 'Agosto', '49 Mallard Circle', '60380', 'Toledo', '0151791678', 'abilovus3p@loc.gov', '1992-05-02', '943669001427', '2012-01-17'),
(135, 'Cassells', 'Sharla', '3413 Hudson Center', '38208', 'Chvalšiny', '0706359778', 'scassells3q@slate.com', '1973-09-30', '052682821227', '1996-12-08');

-- --------------------------------------------------------

--
-- Structure de la table `locations`
--

CREATE TABLE `locations` (
  `numero_resa` bigint(20) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_voiture` int(11) NOT NULL,
  `date_resa` date NOT NULL,
  `date_depart` datetime NOT NULL,
  `date_retour` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `locations`
--

INSERT INTO `locations` (`numero_resa`, `id_client`, `id_voiture`, `date_resa`, `date_depart`, `date_retour`) VALUES
(1, 1, 55, '2020-01-08', '2020-01-29 10:25:00', '2020-01-29 11:30:00'),
(2, 15, 29, '2002-01-08', '2020-01-15 14:30:00', '2020-01-15 16:00:00'),
(3, 106, 12, '2002-01-08', '2020-01-19 12:00:00', '2020-01-19 16:00:00'),
(4, 3, 56, '2020-01-08', '2020-01-29 11:45:00', '2020-01-29 13:30:00'),
(5, 85, 55, '2002-01-08', '2020-01-18 15:30:00', '2020-01-18 19:00:00'),
(6, 38, 38, '2002-01-08', '2020-01-10 12:00:00', '2020-01-10 16:00:00'),
(7, 39, 56, '2020-01-08', '2020-01-30 08:40:00', '2020-01-30 11:30:00'),
(8, 19, 57, '2002-01-08', '2020-01-30 08:30:00', '2020-01-30 19:00:00'),
(9, 116, 19, '2002-01-08', '2020-01-12 12:00:00', '2020-01-12 16:00:00'),
(10, 18, 19, '2020-01-08', '2020-01-29 11:15:00', '2020-01-29 15:30:00'),
(11, 47, 20, '2002-01-08', '2020-01-15 15:20:00', '2020-01-15 16:00:00'),
(12, 36, 55, '2002-01-09', '2020-01-20 12:00:00', '2020-01-20 16:00:00'),
(13, 42, 55, '2020-01-09', '2020-01-27 09:25:00', '2020-01-27 17:30:00'),
(14, 50, 56, '2002-01-09', '2020-01-14 08:30:00', '2020-01-14 13:00:00'),
(15, 100, 13, '2002-01-09', '2020-01-19 13:45:00', '2020-01-19 16:00:00'),
(16, 12, 20, '2020-01-09', '2020-01-27 10:25:00', '2020-01-27 13:30:00'),
(17, 107, 9, '2002-01-09', '2020-01-15 09:30:00', '2020-01-15 16:00:00'),
(18, 122, 28, '2002-01-10', '2020-01-12 12:00:00', '2020-01-12 16:00:00'),
(19, 79, 39, '2020-01-10', '2020-01-22 10:25:00', '2020-01-22 12:15:00'),
(20, 76, 41, '2002-01-10', '2020-01-15 14:30:00', '2020-01-15 18:00:00'),
(21, 89, 56, '2002-01-10', '2020-01-13 12:00:00', '2020-01-13 19:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `modele`
--

CREATE TABLE `modele` (
  `id_modele` mediumint(9) NOT NULL,
  `id_types` tinyint(4) NOT NULL,
  `constructeur` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `chevaux` int(11) DEFAULT NULL,
  `combustible` varchar(255) DEFAULT NULL,
  `consommation` decimal(4,2) DEFAULT NULL,
  `volume_coffre` smallint(6) DEFAULT NULL,
  `nb_places` tinyint(4) DEFAULT NULL,
  `nb_portes` tinyint(4) DEFAULT NULL,
  `boite_vitesse` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `modele`
--

INSERT INTO `modele` (`id_modele`, `id_types`, `constructeur`, `nom`, `chevaux`, `combustible`, `consommation`, `volume_coffre`, `nb_places`, `nb_portes`, `boite_vitesse`) VALUES
(1, 1, 'Ford', 'Fiesta VI 1.0 ECOBOOST 95 S&S CONNECT BUSINESS NAV 3P', 95, 'essence', '4.10', 292, 5, 3, 'manuelle'),
(2, 1, 'Peugeot', '108 1.0 VTI 72 S&S 3CV ALLURE 5P 3P', 72, 'essence', '4.10', 196, 4, 5, 'manuelle'),
(3, 2, 'Ford', 'Focus IV ACTIVE 1.5 ECOBOOST 150 S&S BUSINESS 3P', 150, 'essence', '4.90', 341, 5, 5, 'manuelle'),
(4, 2, 'Tesla', 'Model 3 Standard RWD Plus', 306, 'électrique', NULL, 425, 5, 4, 'automatique'),
(5, 3, 'Volkswagen', 'Passat 8 Alltrack 2.0 TDI 190 4WD DSG7', 190, 'diesel', '5.10', 650, 5, 5, 'automatique'),
(6, 4, 'Renault', 'Grand Scenic IV 1.2 TCE 130 ENERGY ZEN 7PL DSG7', 130, 'essence', '6.10', 189, 7, 5, 'manuelle'),
(7, 5, 'Renault', 'Kangoo Z.E. Extra R-Link', 60, 'électrique', NULL, 3000, 2, 3, 'automatique'),
(8, 6, 'Peugeot', 'Traveller 1.6 BlueHDi 115ch Standard Allure S&S', 116, 'diesel', '5.20', 507, 8, 5, 'manuelle'),
(9, 7, 'Volkswagen', 'California VI (2) 2.0TDI 150 4WD COAST DSG7', 150, 'diesel', '7.40', NULL, 4, 4, 'automatique'),
(10, 8, 'Ford', 'Ranger III 2.0 ECOBLUE 170 7CV S/S SUPER CABINE LIMITED', 170, 'diesel', '7.20', NULL, 4, 4, 'manuelle'),
(11, 9, 'Peugeot', '3008 II 1.6 HYBRID4 300 GT LINE E-EAT8', 300, 'hybride', NULL, 520, 5, 5, 'automatique'),
(12, 10, 'Mazda', 'MX5 IV RF 1.5 SKYACTIV-G EVAP 132 SELECTION RF', 132, 'essence', '6.10', 127, 2, 2, 'manuelle'),
(13, 10, 'Abarth', '124 Spider II 1.4 TURBO 170 TURISMO', 170, 'eseence', '6.40', 140, 2, 2, 'manuelle'),
(14, 11, 'Mercedes', 'Classe S 7 VII (2) 450 390 FASCINATION 4MATIC LIMOUSINE', 367, 'hybride', '8.00', 510, 5, 4, 'automatique');

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

CREATE TABLE `types` (
  `id_types` tinyint(4) NOT NULL,
  `libelle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `types`
--

INSERT INTO `types` (`id_types`, `libelle`) VALUES
(1, 'citadine'),
(2, 'berline'),
(3, 'break'),
(4, 'monospace'),
(5, 'fourgonnette'),
(6, 'camionnette'),
(7, 'camping-car'),
(8, 'pickup'),
(9, '4x4'),
(10, 'cabriolet'),
(11, 'limousine');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `identifiant` varchar(100) NOT NULL,
  `mdp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `identifiant`, `mdp`) VALUES
(1, 'christine', 'coucou');

-- --------------------------------------------------------

--
-- Structure de la table `voiture`
--

CREATE TABLE `voiture` (
  `id_voiture` int(11) NOT NULL,
  `id_modele` mediumint(9) NOT NULL,
  `couleur` varchar(255) NOT NULL,
  `immatriculation` char(9) NOT NULL,
  `louable` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `voiture`
--

INSERT INTO `voiture` (`id_voiture`, `id_modele`, `couleur`, `immatriculation`, `louable`) VALUES
(1, 1, 'bleu azur métallisé', 'FP-578-JS', 1),
(2, 1, 'bleu abysse uni', 'FP-503-JS', 1),
(3, 2, 'gris gallium métallisé', 'FP-671-JS', 1),
(4, 2, 'rouge scarlet uni', 'FP-694-JS', 1),
(5, 2, 'gris gallium métallisé', 'FP-732-JS', 1),
(6, 2, 'rouge scarlet uni', 'FP-778-JS', 1),
(7, 2, 'blanc lipizan', 'FP-795-JS', 1),
(8, 2, 'gris gallium métallisé', 'FP-671-JS', 1),
(9, 3, 'bleu azur métallisé', 'FP-029-JT', 1),
(10, 3, 'blanc metropolis métallisé', 'FP-077-JT', 1),
(11, 3, 'noir shadow mica', 'FP-108-JT', 1),
(12, 3, 'bleu azur métallisé', 'FP-130-JT', 1),
(13, 3, 'bleu azur métallisé', 'FP-187-JT', 1),
(14, 3, 'rouge racing uni', 'FP-209-JT', 1),
(15, 3, 'blanc metropolis métallisé', 'FP-237-JT', 1),
(16, 3, 'noir shadow mica', 'FP-268-JT', 1),
(17, 3, 'bleu azur métallisé', 'FP-287-JT', 1),
(18, 3, 'blanc metropolis métallisé', 'FP-306-JT', 1),
(19, 4, 'pearl white', 'FP-413-JT', 1),
(20, 4, 'midnight silver', 'FP-485-JT', 1),
(21, 5, 'blanc oryx', 'FP-492-JT', 1),
(22, 5, 'gris urano', 'FP-527-JT', 1),
(23, 5, 'gris urano', 'FP-545-JT', 1),
(24, 5, 'bleu atlantique', 'FP-569-JT', 1),
(25, 5, 'bleu atlantique', 'FP-586-JT', 1),
(26, 5, 'gris urano', 'FP-601-JT', 1),
(27, 5, 'bleu atlantique', 'FP-627-JT', 1),
(28, 5, 'blanc oryx', 'FP-648-JT', 1),
(29, 6, 'beige dune', 'FP-704-JT', 1),
(30, 6, 'beige dune', 'FP-723-JT', 1),
(31, 6, 'beige dune', 'FP-748-JT', 1),
(32, 6, 'blanc glacier/noir étoilé', 'FP-769-JT', 1),
(33, 6, 'beige dune', 'FP-802-JT', 1),
(34, 6, 'rouge carmin/noir étoilé', 'FP-823-JT', 1),
(35, 6, 'rouge carmin/noir étoilé', 'FP-857-JT', 1),
(36, 6, 'rouge carmin/noir étoilé', 'FP-888-JT', 1),
(37, 6, 'blanc glacier/noir étoilé', 'FP-905-JT', 1),
(38, 6, 'rouge carmin/noir étoilé', 'FP-934-JT', 1),
(39, 7, 'rouge pavot', 'FP-237-JV', 1),
(40, 7, 'rouge pavot', 'FP-260-JV', 1),
(41, 7, 'bleu étoile', 'FP-289-JV', 1),
(42, 7, 'bleu étoile', 'FP-301-JV', 1),
(43, 7, 'gris higland', 'FP-325-JV', 1),
(44, 8, 'blanc banquise', 'FP-347-JV', 1),
(45, 8, 'blanc banquise', 'FP-368-JV', 1),
(46, 8, 'blanc banquise', 'FP-381-JV', 1),
(47, 8, 'blanc banquise', 'FP-399-JV', 1),
(48, 8, 'orange tourmaline', 'FP-427-JV', 1),
(49, 9, 'vert bambouseraie métallisé', 'FP-446-JV', 1),
(50, 10, 'rouge colorado', 'FP-467-JV', 1),
(51, 10, 'blanc glacier', 'FP-493-JV', 1),
(52, 10, 'rouge colorado', 'FP-512-JV', 1),
(53, 11, 'rouge ultimate teinte spéciale', 'FP-557-JV', 1),
(54, 11, 'metallic copper (cuprite) métallisé', 'FP-588-JV', 1),
(55, 12, 'blue reflex Mica', 'FP-634-JV', 1),
(56, 13, 'rouge costa brava 1972 pastel', 'FP-759-JV', 1),
(57, 14, 'noir obsidienne metallic', 'FP-884-JV', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`);

--
-- Index pour la table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`numero_resa`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `id_voiture` (`id_voiture`);

--
-- Index pour la table `modele`
--
ALTER TABLE `modele`
  ADD PRIMARY KEY (`id_modele`),
  ADD KEY `id_types` (`id_types`);

--
-- Index pour la table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id_types`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Index pour la table `voiture`
--
ALTER TABLE `voiture`
  ADD PRIMARY KEY (`id_voiture`),
  ADD KEY `id_modele` (`id_modele`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT pour la table `locations`
--
ALTER TABLE `locations`
  MODIFY `numero_resa` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `modele`
--
ALTER TABLE `modele`
  MODIFY `id_modele` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `types`
--
ALTER TABLE `types`
  MODIFY `id_types` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `voiture`
--
ALTER TABLE `voiture`
  MODIFY `id_voiture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `locations_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `client` (`id_client`),
  ADD CONSTRAINT `locations_ibfk_2` FOREIGN KEY (`id_voiture`) REFERENCES `voiture` (`id_voiture`);

--
-- Contraintes pour la table `modele`
--
ALTER TABLE `modele`
  ADD CONSTRAINT `modele_ibfk_1` FOREIGN KEY (`id_types`) REFERENCES `types` (`id_types`);

--
-- Contraintes pour la table `voiture`
--
ALTER TABLE `voiture`
  ADD CONSTRAINT `voiture_ibfk_1` FOREIGN KEY (`id_modele`) REFERENCES `modele` (`id_modele`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
