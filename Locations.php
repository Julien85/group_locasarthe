
<?php

class Locations
{
    private $numero_resa;
    private $id_client;
    private $date_resa;
    private $date_depart;
    private $date_retour;

    public function __construct($numero_resa_constr, $id_client_constr, $date_resa_constr, $date_depart_constr,$date_retour_constr)
    {
        $this->numero_resa = $numero_resa_constr;
        $this->id_client = $id_client_constr;
        $this->date_resa = $date_resa_constr;
        $this->date_depart = $date_depart_constr;
        $this->date_retour = $date_retour_constr;
    }

    public function getNum_resa(){
        return $this->numero_resa;
    }

    public function setNum_resa($Numero_resa_saisie){
         $this->numero_resa = $Numero_resa_saisie;
    }

    public function getid_client(){
        return $this->id_client;
    }

    public function setId_client($Id_client_saisie){
         $this->id_client = $Id_client_saisie;
    }

    public function getdate_resa(){
        return $this->date_resa;
    }

    public function setDate_resa($Date_resa_saisie){
         $this->date_resa = $Date_resa_saisie;
    }

    public function getdate_depart(){
        return $this->date_depart;
    }

    public function setDate_depart($Date_depart_saisie){
         $this->date_depart = $Date_depart_saisie;
    }

    public function getdate_retour(){
        return $this->date_retour;
    }

    public function setDate_retour($Date_retour_saisie){
         $this->date_retour = $Date_retour_saisie;
    }
}



?>
