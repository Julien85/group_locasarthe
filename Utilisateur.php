<?php

class Utilisateur {
    //Attributes
    private $id;
    private $pseudo;
    private $mdp;

    //Constructor
    public function __construct($pseudo_constr, $mdp_constr){
        $this->pseudo = $pseudo_constr;
        $this->mdp = $mdp_constr;
    }

    //Getters & Setters
    public function getId(){
        return $this->id;
    }
    public function setId($id_saisie){
        $this->id = $id_saisie;
    }
    public function getPseudo(){
        return $this->pseudo;
    }
    public function setPseudo($pseudo_saisie){
        $this->pseudo = $pseudo_saisie;
    }
    public function getMdp(){
        return $this->mdp;
    }
    public function setMdp($mdp_saisie){
        $this->mdp = $mdp_saisie;
    }
}