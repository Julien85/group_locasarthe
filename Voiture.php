<?php

class Voiture{
    private $id_voiture;
    private $id_modele;
    private $couleur;
    private $immatriculation;
    private $louable;

    //Constructor
    public function __construct($couleur_constr, $immatriculation_constr, $louable_constr){
        $this->couleur = $couleur_constr;
        $this->immatriculation = $immatriculation_constr;
        $this->louable = $louable_constr;
    }
    

    //Getters & Setters
    public function getId_voiture(){
        return $this->id_voiture;
    }
    public function setId($id_voiture){
        $this->id_voiture = $voiture_saisie;
    }
    public function getId_modele(){
        return $this->id_modele;
    }
    public function setId_modele($id_modele){
        $this->id_modele = $modele_saisie;
    }
    public function getCouleur(){
        return $this->couleur;
    }
    public function setCouleur($couleur){
        $this->couleur = $couleur_saisie;
    }
    public function getImmatriculation(){
        return $this->immatriculation;
    }
    public function setImmatriculation($immatriculation){
        $this->immatriculation = $immatriculation_saisie;
    }
    public function getLouable(){
        return $this->louable;
    }
    public function setLouable($louable){
        $this->louable = $louable_saisie;
    }

    
}