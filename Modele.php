<?php

class Modele {
    private $id;
    private $type;
    private $constructeur;
    private $nom;
    private $cheveaux;
    private $combustible;
    private $consommation;
    private $volumeCoffre;
    private $nbPlace;
    private $nbPorte;
    private $boiteVitesse;

    //Constructor
    public function __construct($id_constr, $type_constr, $constructeur_constr, $nom_constr, $cheveaux_constr, $combustible_constr, $consommation_constr, $volumeCoffre_constr, $nbPlace_constr, $nbPorte_constr, $boiteVitesse_constr){
        $this->id = $id_constr;
        $this->type = $type_constr;
        $this->constructeur = $constructeur_constr;
        $this->nom = $nom_constr;
        $this->cheveaux = $cheveaux_constr;
        $this->combustible = $combustible_constr;
        $this->consommation = $consommation_constr;
        $this->volumeCoffre = $volumeCoffre_constr;
        $this->nbPlace = $nbPlace_constr;
        $this->nbPorte = $nbPorte_constr;
        $this->boiteVitesse = $boiteVitesse_constr;
    }

    //Getters & Setters
    public function getId(){
        return $this->id;
    }
    public function setId($id_saisie){
        $this->id = $id_saisie;
    }

    public function getType(){
        return $this->type;
    }
    public function setType($type_saisie){
        $this->type = $type_saisie;
    }

    public function getNom(){
        return $this->nom;
    }
    public function setNom($nom_saisie){
        $this->nom = $nom_saisie;
    }

    public function getCheveaux(){
        return $this->cheveaux;
    }
    public function setCheveaux($cheveaux_saisie){
        $this->cheveaux = $cheveaux_saisie;
    }

    public function getCombustible(){
        return $this->combustible;
    }
    public function setCombustible($combustible_saisie){
        $this->combustible = $combustible_saisie;
    }

    public function getConsommation(){
        return $this->consommation;
    }
    public function setConsommation($consommation_saisie){
        $this->consommation = $consommation_saisie;
    }

    public function getVolumeCoffre(){
        return $this->volumeCoffre;
    }
    public function setVolumeCoffre($volumeCoffre_saisie){
        $this->volumeCoffre = $volumeCoffre_saisie;
    }

    public function getNbPlace(){
        return $this->nbPlace;
    }
    public function setNbPlace($nbPlace_saisie){
        $this->nbPlace = $nbPlace_saisie;
    }

    public function getNbPorte(){
        return $this->nbPorte;
    }
    public function setNbPorte($nbPorte_saisie){
        $this->nbPorte = $nbPorte_saisie;
    }

    public function getBoiteVitesse(){
        return $this->boiteVitesse;
    }
    public function setBoiteVitesse($boiteVitesse_saisie){
        $this->boiteVitesse = $boiteVitesse_saisie;
    }

}




